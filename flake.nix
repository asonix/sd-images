{
  description = "A very basic flake";

  # pin this to unstable
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixos-aarch64-images = {
      url = "git+https://git.asonix.dog/asonix/nixos-aarch64-images";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, nixos-aarch64-images }:
    let
      asonix-key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3+mNUlokSKZQNXJAuGm2LCHelKuElWLJArzIYZQYEPbrFaE+J8VtfNbMMD1qVI21ksfcqvFQW4aiP4+BFDxTOGW0uBmUHWKxkyyU39y2yhnsa+svwwIooc+Iwkxw0atzSMEBb94UaZlq9cKMSnG9RGeRFqfYnW2s49wpU79wk6zEFUuOHCMKn4R7zqkPac7IyjxZeKlspY3fOasNH4zyrkbhEOlvrwEOdRNTRNCWWzDcinIVZjfmErHlSynshx9yLnCGkLBxHSxgI2TVyR3RlQ3aGbHtB3QN5X7/T/dwXJFJ11P1Q2bC3XP3hHCogDqXcPvDTFSQEM/mZuFcKNbsn asonix@asonix-tower";
      trusted-public-keys = "firestar:spmMw07mO3cxflq5g2GazhE7ddgEoz6QLwaiCnyz/fg=";

      customized = ({ lib, ... }: {
        users.users.asonix = {
          isNormalUser = true;
          description = "Tavi";
          extraGroups = [ "wheel" ];
          openssh.authorizedKeys.keys = [ asonix-key ];
          initialPassword = "changeme";
        };

        # Nix config
        nix.extraOptions = ''
          trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= ${trusted-public-keys}
        '';

        services.openssh = {
          enable = true;
          settings.PermitRootLogin = lib.mkForce "prohibit-password";
        };
      });

      buildConfig = {
        kernel = nixos-aarch64-images.packages.x86_64-linux.kernels.linux_6_2-rockchip;
        modules = [
          customized
        ];
      };
    in
    with nixos-aarch64-images.packages.x86_64-linux;
    {
      packages.x86_64-linux = {
        modules = (buildModules buildConfig) // { inherit customized; };

        quartz64a = buildQuartz64A buildConfig;
        quartz64b = buildQuartz64B buildConfig;
        soquartz-blade = buildSoQuartzBlade buildConfig;
        soquartz-cm4 = buildSoQuartzCM4 buildConfig;
        soquartz-model-a = buildSoQuartzModelA buildConfig;
        rock64 = buildRock64 buildConfig;
        rockPro64 = buildRockPro64 buildConfig;
        rockPro64v2 = buildRockPro64v2 buildConfig;
        rock-pc-rk3399 = buildRockPCRk3399 buildConfig;
        pinebook-pro = buildPinebookPro buildConfig;
      };
    };
}
